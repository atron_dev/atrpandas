#!/usr/bin/env python

#pandrev-mongodb-ver.1
import pandas as pd
import numpy as np
import time
from influxdb import InfluxDBClient
from config import *
from apscheduler.schedulers.background import BackgroundScheduler
from pymongo import MongoClient

"""
InfluxDBClient initiation.
"""
client=InfluxDBClient(influx_hostname,influx_port,influx_user,influx_password,influx_dbname)

"""
Scheduler initiation.
"""
scheduler = BackgroundScheduler()

"""
Mongodb connection
"""
client_init = MongoClient(mongo_hostname,27017,username=mongo_user,password=mongo_password) 
mongodb = client_init['atron_monitoring_db']
pd.options.mode.chained_assignment = None 

def occupancy_processing():
    print("Start Processing")
    global_start = time.time()
    """
    InfluxDB's query get result.
    took about 3s-5s to done.
    """
    query_ = "select ifHCInOctets, ifHCOutOctets, hostname, ifName, ifDescr,agent_host from interface where ifHCInOctets != 0 and ifHCOutOctets != 0 and time >= now()-12m"
    start = time.time()
    res_nquery = client.query(query_)
    points_nquery = res_nquery.get_points()
    indexs_nquery = res_nquery.keys()
    end = time.time()
    print(" Influxdb query elapsed time : {}".format(end-start))
    
    """
    InfluxDB keys and points convertion into DataFrame.
    """
    df_influx = pd.DataFrame(points_nquery)
    df_influx_temp = df_influx['ifDescr'].str.contains('Huawei', na=False, regex=True)
    df_influx_h = df_influx[df_influx_temp == True]
    df_influx_z = df_influx[df_influx_temp == False]

    values = {'hostname': 'Null', 'ifName': 'Null', 'ifDescr': 'Null'}
    df_influx_z = df_influx_z.fillna(value=values)
    df_influx_z['port_uplink'] = df_influx_z['ifDescr']
    df_influx_z = df_influx_z[['agent_host','hostname','ifHCInOctets','ifHCOutOctets','time','port_uplink']]
    df_influx_h = df_influx_h.fillna(value=values)
    df_influx_h_null = df_influx_h[df_influx_h['ifHCInOctets'] == 9223372036854775807]
    df_influx_h = df_influx_h.drop(df_influx_h_null.index)
    df_influx_h['port_uplink'] = df_influx_h['ifName']
    df_influx_h = df_influx_h[['agent_host','hostname','ifHCInOctets','ifHCOutOctets','time','port_uplink']]

    """
    InfluxDB keys and points merge.
    """
    df_influx_h_max = df_influx_h[df_influx_h.groupby(['time','agent_host'])['ifHCInOctets'].transform(max) == df_influx_h['ifHCInOctets']]
    df_influx_z_max = df_influx_z[df_influx_z.groupby(['time','agent_host'])['ifHCInOctets'].transform(max) == df_influx_z['ifHCInOctets']]
    df_influx_max = pd.concat([df_influx_h_max,df_influx_z_max])
    df_influx_max = df_influx_max.reset_index()
    df_influx_max['time'] = pd.to_datetime(df_influx_max['time'])
    df_influx_max['time'] = df_influx_max['time'].dt.tz_convert('Asia/Jakarta').dt.floor('5min')
    df_influx_max['time'] = df_influx_max['time'].dt.tz_localize(None)
    df_influx_max = df_influx_max.drop(columns='index')
    
    """
    Differentiate between two intervals data (up) traffic and only one traffic data (down)
    """
    counter = df_influx_max.groupby(['agent_host']).count()
    up = counter[counter['hostname']!=1].reset_index()
    up = up['agent_host'].reset_index().drop(columns='index')
    down = counter[counter['hostname']==1].reset_index()

    """
    Get derivative from two intervals data traffic and merge it
    """
    df_influx_temp = pd.merge(df_influx_max,up,on='agent_host')
    temp_max = df_influx_temp.groupby(['agent_host','port_uplink',"hostname"])['ifHCInOctets','ifHCOutOctets','time'].max().reset_index().rename(index=str, columns={"ifHCInOctets": "max_in_bit","ifHCOutOctets":"max_out_bit","port_uplink":"port_uplink_max","time":"time_max","hostname":"hostname_max"})
    temp_min = df_influx_temp.groupby(['agent_host','port_uplink',"hostname"])['ifHCInOctets','ifHCOutOctets','time'].min().reset_index().rename(index=str, columns={"ifHCInOctets": "min_in_bit","ifHCOutOctets":"min_out_bit","port_uplink":"port_uplink_min","time":"time_min","hostname":"hostname_min"})
    temp = pd.merge(temp_max,temp_min,on='agent_host')

    """
    Filtering an incomplete data, the maximum on such interface are not found in the next/before time interval
    """
    incomplete = temp[temp['port_uplink_max'] != temp['port_uplink_min']]
    temp = temp.drop(incomplete.index)

    """
    Occupancy calculation
    """
    temp['in_bit'] = temp['max_in_bit'] - temp['min_in_bit']
    temp['out_bit'] = temp['max_out_bit'] - temp['min_out_bit']
    temp['in_mbps'] = temp['in_bit']*8/300/1000000
    temp['out_mbps'] = temp['out_bit']*8/300/1000000

    """
    Drop anomaly occupancy (more than 10Gbps)
    """
    df_traf = temp[['agent_host','in_mbps','time_max','port_uplink_max','hostname_max']]
    anomaly = df_traf[(df_traf['in_mbps']>10000) | (df_traf['in_mbps']==0) ]
    df_traf = df_traf.drop(anomaly.index)
    df_traf = df_traf.rename(columns={"agent_host":"ip_ont","time_max":"timestamp","port_uplink_max":"port_uplink","hostname_max":"hostname"})
 
    """
    Read basic info data from mongodb
    """
    col = mongodb['basic_info']
    query = col.find({},{ "_id": 0, "site_id": 1, "ip_ont": 1, "bw_current" : 1 })
    bw_data = pd.DataFrame(list(query))

    '''
    Convert bw_tps from Mb to bit
    '''
    bw_data['bw_current'] = pd.to_numeric(bw_data['bw_current'])

    '''
    Merge bw_data and influx data, occupancy calculation and group by each maximum occupancy per site id.
    '''

    occupancy = pd.merge(bw_data,df_traf,on='ip_ont')
    occupancy['current_occ'] = occupancy['in_mbps']*100/occupancy['bw_current']
    occupancy = occupancy.round({'current_occ':2})
    occupancy = occupancy.drop(columns=['bw_current'])
    occupancy['m'] = occupancy['timestamp'].dt.minute
    occupancy['timestamp'] = occupancy['timestamp'].dt.floor('H',nonexistent='shift_backward')
    

    '''
    Preparing & reformatting to JSON periodic_data documents
    '''
    periodic_data = occupancy[['site_id','current_occ','timestamp','m']]
    periodic_data_todb = periodic_data.to_dict(orient='records')

    '''
    Preparing & reformatting to JSON additional_info documents
    '''
    additional_info = occupancy[['site_id','port_uplink','hostname','timestamp']]
    additional_info_todb = additional_info.to_dict(orient='records')

    '''
    Updating additional_info documents to mongodb
    '''
    bulk = mongodb.additional_info.initialize_unordered_bulk_op()
    print("Updating additional info documents")
    start = time.time()
    for i in range(len(additional_info_todb)):
        bulk.find({"site_id":additional_info_todb[i]['site_id']}).upsert().update_one({"$set" : {"port_uplink":additional_info_todb[i]['port_uplink'], "hostname":additional_info_todb[i]['hostname'], "timestamp":additional_info_todb[i]['timestamp']}})
    bulk.execute()
    end = time.time()
    print(" Update additional info elapsed time : {}".format(end-start))

    '''
    Updating additional_info documents to mongodb
    '''
    bulk = mongodb.new_periodic_data.initialize_unordered_bulk_op()
    print("Updating periodic data collection")
    start = time.time()
    for i in range(len(periodic_data_todb)):
        data = {"minutes":periodic_data_todb[i]['m'],"occ":periodic_data_todb[i]['current_occ']}
        bulk.find({"site_id":periodic_data_todb[i]['site_id'],"dt":periodic_data_todb[i]['timestamp']}).upsert().update_one({"$push" : {"data":data}})
    bulk.execute()
    end = time.time()
    print(" Update periodic data elapsed time : {}".format(end-start))

    print("End of global processing")
    global_end = time.time()
    print("Global elapsed time : {}".format(global_end-global_start))

def main():
    # occupancy_processing()
    job = scheduler.add_job(occupancy_processing,trigger='cron', minute="*/5")
    scheduler.start()
    while True:
        time.sleep(1)

if __name__ == "__main__":
    main()

